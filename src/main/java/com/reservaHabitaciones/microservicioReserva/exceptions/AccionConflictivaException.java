package com.reservaHabitaciones.microservicioReserva.exceptions;

public class AccionConflictivaException extends RuntimeException {
    public AccionConflictivaException(String message) {
        super(message);
    }
}
